import { Component } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import * as _ from 'lodash';
import { HttpClient, HttpHeaderResponse,HttpResponse } from '@angular/common/http';
import { map, retry, catchError,tap } from 'rxjs/operators';
import { Observable,throwError  } from 'rxjs';


  @Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Oauth2test';
  private tokendecode: any;
  private decodingHeaders: any;
  private linkUrl:any;
  private headerProperty:any;
  private linkUrl2:any;
  private daStampare:any;  
  
  
  constructor(private http:HttpClient ) {}

    nonceRandom(){
      var num = Math.floor(Math.random() * 90000) + 10000;

      this.linkUrl2="http://svilohs.risorse.int:7777/ms_oauth/oauth2/endpoints/GSEIdentityDomainserviceprofile/authorize?client_id=ac72d4e0fe4a4239b5edcc3923fbacc3&response_type=token&redirect_uri=http://10.160.22.17/oauth2test/&scope=APIGSE.BASIC&state=" + num;
      
      this.http.get<object>(this.linkUrl, {observe: 'response'}).subscribe(resp => {
      console.log(resp.headers);
   });
    }


    ngOnInit() {
    
      var url = window.location.href,
      access_token = url.match(/\#(?:access_token)\=([\S\s]*?)\&/)[1];
    
      var splittedtoken=_.split(access_token, '.', 1);

      this.decodingHeaders=JSON.parse(atob(splittedtoken));

      this.tokendecode=jwt_decode(access_token);
      this.daStampare=this.nonceRandom();
  }
}
